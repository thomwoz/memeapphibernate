package pl.akademiakodu.memy.model;

import javax.persistence.*;

@Entity
public class Gif {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private boolean favorite;
    private String name;
    private String gifUrl;

    @OneToOne
    private Category category;


    public Category getCategory() {
        return category;
    }

    public Gif(boolean favorite, String gifUrl, String name, Category category) {
        this.favorite = favorite;
        this.gifUrl = gifUrl;
        this.name=name;
        this.category=category;
    }

    public Gif() {
    }

    public Gif(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Gif(String name) {
        this.name = name;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

//    public Gif(boolean favorite, String gifUrl, String name) {
//        this.favorite = favorite;
//        this.gifUrl = gifUrl;
//        this.name=name;
//    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }



//    public Gif(String gifUrl, String name) {
//        this.gifUrl = gifUrl;
//        this.name=name;
//    }

    public String getGifUrl() {
        return gifUrl;
    }

    public void setGifUrl(String gifUrl) {
        this.gifUrl = gifUrl;
    }

    @Override
    public String toString() {
        return gifUrl;
    }
}
