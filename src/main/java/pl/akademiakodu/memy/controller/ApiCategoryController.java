package pl.akademiakodu.memy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.akademiakodu.memy.dao.CategoryDao;
import pl.akademiakodu.memy.model.Category;

import java.util.ArrayList;
import java.util.List;


@RestController
public class ApiCategoryController {

    @Autowired
    CategoryDao categoryDao;

    private static final String template = "%s!";

    @RequestMapping("api/categories/{id}")
    public Category category(@PathVariable Long id) {
        Category cat=categoryDao.findById(id).get();
        if(cat!=null) return cat;
        else return new Category(
                "BRAK KATEGORII");
    }

    @RequestMapping("api/categories")
    public Iterable<Category> categories() {
//        Category kategoria=new Category();
        Iterable<Category> categories=categoryDao.findAll();
        if(categories==null)
         {
             List<Category> cat=new ArrayList<>();
             cat.add(new Category("BRAK KATEGORII"));
             return cat;
        }
        return categories;
    }
}