package pl.akademiakodu.memy.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.akademiakodu.memy.dao.GifDao;
import pl.akademiakodu.memy.model.Gif;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ApiGifController {

    @Autowired
    GifDao gifDao;

    private static final String template = "%s!";

    @RequestMapping("api/gifs")
    public Iterable<Gif> gifs() {
        Iterable<Gif> gifs=gifDao.findAll();
        if(gifs==null)
        {   List<Gif> list=new ArrayList<>();
            list.add(new Gif("BRAK GIFÓW"));
            return list;
        }
        return gifs;
    }

    @RequestMapping("/changefavorite")
    public Gif changeFavorites(@RequestParam Long id){
        System.out.println(id);
        Gif gif=gifDao.findById(id).get();
        gif.setFavorite(!gif.isFavorite());
        gifDao.save(gif);
        System.out.println(gif.isFavorite());
        return new Gif();
    }

}