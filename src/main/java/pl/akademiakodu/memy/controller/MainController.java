package pl.akademiakodu.memy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import pl.akademiakodu.memy.dao.CategoryDao;
import pl.akademiakodu.memy.dao.GifDao;
import pl.akademiakodu.memy.model.Category;
import pl.akademiakodu.memy.model.Gif;

import java.util.ArrayList;
import java.util.List;

@Controller
public class MainController {

//    private Facebook facebook;
//    private ConnectionRepository connectionRepository;
//
//    public MainController(Facebook facebook, ConnectionRepository connectionRepository) {
//        this.facebook = facebook;
//        this.connectionRepository = connectionRepository;
//    }
//
//    @GetMapping("/face")
//    public String helloFacebook(ModelMap modelMap) {
//        if (connectionRepository.findPrimaryConnection(Facebook.class) == null) {
//            return "redirect:/connect/facebook";
//        }
//
//        modelMap.addAttribute("facebookProfile", facebook.userOperations().getUserProfile());
//        PagedList<Post> feed = facebook.feedOperations().getFeed();
//        modelMap.addAttribute("feed", feed);
//        return "hello";
//    }


    @Autowired
    GifDao gifDao;

    @Autowired
    CategoryDao categoryDao;


    @GetMapping("/login")
    public String login(){
        return "login";
    }

    @GetMapping("/403")
    public String error403() {
        return "403";
    }

    @GetMapping("/")
    public String showHome(ModelMap modelMap){
        modelMap.addAttribute("gifs", gifDao.findAll());
        return "home";
    }

    @GetMapping("/favorites")
    public String showFavorites(ModelMap modelMap){
        modelMap.addAttribute("gifs", gifDao.findByFavoriteTrue());
        return "favorites";
    }



    @GetMapping("/detail/{name}")
    public String showGif(@PathVariable String name, ModelMap modelMap){
        modelMap.addAttribute("gif", gifDao.findByName(name));
        return "gif-details";
    }

    @GetMapping("/categories")
    public String showGif(ModelMap modelMap){
        modelMap.addAttribute("categories", categoryDao.findAll());
        return "categories";
    }

    @GetMapping("/category/{id}")
    public String showGif(@PathVariable Long id, ModelMap modelMap){
        modelMap.addAttribute("category",categoryDao.findById(id).get());

        modelMap.addAttribute("gifs", gifDao.findByCategory(categoryDao.findById(id).get()));

        return "category";
    }

    @GetMapping("/admin/addcategory")
    public String addCategory(){
        return "addcategory";
    }

    @GetMapping("/user/addmeme")
    public String addMeme(ModelMap modelMap){
        System.out.println(categoryDao.findAll());
        modelMap.addAttribute("categories", categoryDao.findAll());
        return "addmeme";
    }

    @PostMapping("/user/addconfirmation")
    public String showMemeConfirmation(@ModelAttribute Gif gif, ModelMap modelMap){
        gifDao.save(gif);
        return "addconfirmation";
    }

    @PostMapping("/admin/addconfirmation")
    public String showConfirmation(@ModelAttribute Category category, ModelMap modelMap){
        categoryDao.save(category);
        return "addconfirmation";
    }

    @GetMapping("/search")
    public String searchGif(@RequestParam String q, ModelMap modelMap){
        List<Gif> list=new ArrayList<>();
        for(Gif gif:gifDao.findAll()) {
            if(gif.getName().toLowerCase().equals(q.toLowerCase()) || gif.getCategory().getName().toLowerCase().equals(q.toLowerCase())) list.add(gif);
        }

        modelMap.addAttribute("emptyList",list.isEmpty());
        modelMap.addAttribute("gifs", list);
        return "searchgifs";
    }
}