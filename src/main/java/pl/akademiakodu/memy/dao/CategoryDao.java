package pl.akademiakodu.memy.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.akademiakodu.memy.model.Category;

@Repository
public interface CategoryDao extends CrudRepository<Category, Long> {
    Category findByName(String name);
//    Category searchByName(String name);
}
