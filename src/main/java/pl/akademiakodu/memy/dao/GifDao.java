package pl.akademiakodu.memy.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.akademiakodu.memy.model.Category;
import pl.akademiakodu.memy.model.Gif;

import java.util.List;


public interface GifDao extends CrudRepository<Gif, Long>{
    List<Gif> findByFavoriteTrue();
    Gif findByName(String name);
    List<Gif> findByCategory(Category category);
//    Gif searchByName(String name);
}